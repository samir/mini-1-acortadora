from webapp import WebApp
import shelve
import string
import random
from urllib import parse

contents = shelve.open('contents')

FORM = """
    <hr>
    <form action = "/" method = "post">
      <div>
        <label> Url: </label>
        <input type="text" name="url" required>
      <div>
        <input type="submit" value="Submit">
      </div>
    </form>
"""

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>Content for {resource}:</p> 
    <div>
        {form}
    <div>
        <p>  URL list: <br>
        <div>
        <p>{dic}<p>
    <div>
  </body>
</html>
"""


PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p> Resource not found: {resource}.</p>
    </div>
      {form}
    </div>
        <p> URL list: {dic}<br>
    <div>
  </body>
</html>
"""


PAGE_POST = """
<!DOCTYPE html>
<html lang="en">
  <body>
  <div>
    <p>  URL list: {body}<br>
  <div>
      {form}
  <div>
        <p> URL list: {dic}<br>
  <div>
  </body>
</html>
"""

PAGE_UNPROCESABLE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p> Unprocesable POST: {body}.</p>
  </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p> Method not allowed: {method}.</p>
  </body>
</html>
"""

CODE_REDIRECT = "301 Moved Permanently\r\n" \
 + "Location: {location}\r\n\r\n"


def complete_url(url):
    if "http://" in url or "https://" in url:
        return url
    else:
        return "https://" + url


def return_dic(contents):
    dic = ""
    for i in contents:
        dic = dic + "-->  Url: " + contents[i] + " -->  Shorten url: " + i + "<br>"
    return dic


class RandomShort(WebApp):

    def parse(self, request):
        data = {}
        body_start = request.find('\r\n\r\n')
        if body_start == -1:
            data['body'] = None
        else:
            data['body'] = request[body_start + 4:]
        parts = request.split(' ', 2)
        data['method'] = parts[0]
        data['resource'] = parts[1]
        return data

    def process(self, data):
        if data['method'] == 'GET':
            code, ans = self.get(data['resource'])
        elif data['method'] == 'POST':
            code, ans = self.post(data['resource'], data['body'])
        else:
            code, ans = "405 Method not allowed", \
                PAGE_NOT_ALLOWED.format(method=data['method'])
        return code, ans

    def get(self, resource):
        if resource == '/':
            print(return_dic(contents))
            ans, code = PAGE.format(resource=resource, form=FORM, dic=return_dic(contents)), "200 OK"
        elif resource in contents:
            location = contents[resource]
            ans, code = CODE_REDIRECT.format(location=location), ""
        else:
            ans, code = PAGE_NOT_FOUND.format(resource=resource, form=FORM, dic=return_dic(contents)),\
                        "404 Resource Not Found"
        return code, ans

    def post(self, resource, body):
        fields = parse.parse_qs(body)
        if resource == '/':
            if 'url' in fields:
                url = complete_url(fields['url'][0])
                if url not in contents.values():
                    random_key = ''.join(random.choices(string.ascii_lowercase + string.digits, k=8))
                    short_url = '/' + random_key
                    print(short_url)
                    contents[short_url] = url
                    ans, code = PAGE_POST.format(body=short_url, form=FORM, dic=return_dic(contents)),\
                                "200 OK"
                else:
                    ans, code = PAGE_POST.format(body=list(contents.keys())[list(contents.values()).index(url)], form=FORM, dic=return_dic(contents)), \
                            "200 OK"
            else:
                ans, code = PAGE_UNPROCESABLE.format(body=body),\
                           "422 Unprocessable Entity"
        else:
            ans, code = PAGE_UNPROCESABLE.format(body=body), \
                        "422 Unprocessable Entity"
        return code, ans


if __name__ == "__main__":
    server = RandomShort("localhost", 2907)